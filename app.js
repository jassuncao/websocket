const WebSocket = require("ws");

const wss = new WebSocket.Server({ port: 3030 });

wss.on("connection", function connection(ws) {
  ws.on("message", function incoming(data) {
    wss.clients.forEach(function each(client) {
      if (data === "/") {
        client.send("Comandos disponínel: \n/time \n\n/calc <expressao>");
      } else {
        if (data === "/time") {
          var now = new Date();
          client.send(`data: ${now}`);
        } else if (data.indexOf("/calc") >= 0) {
          var command = data.split(" ");
          client.send(`O resultado de ${command[1]} é: ${eval(command[1])}`);
        } else {
          client.send(
            "Comando não encontrado!!!\nComandos disponínel: \n/time \n\n/calc <expressao>"
          );
        }
      }
    });
  });
});
