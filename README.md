# Implementando [WEBSocket]([https://blog.bitlabstudio.com/a-simple-chat-app-with-react-node-and-websocket-35d3c9835807](https://blog.bitlabstudio.com/a-simple-chat-app-with-react-node-and-websocket-35d3c9835807)) em [React]([https://medium.com/code-prestige/como-criar-um-app-react-consumindo-um-back-end-node-com-express-5030e1727ace](https://medium.com/code-prestige/como-criar-um-app-react-consumindo-um-back-end-node-com-express-5030e1727ace))!

[![node](https://img.shields.io/badge/node-8.10.0-green.svg)]([https://nodejs.org/en/download/](https://nodejs.org/en/download/))
[![yarn](https://img.shields.io/badge/yarn-1.16.0-blue.svg)]([https://yarnpkg.com/pt-BR/docs/install#debian-stable](https://yarnpkg.com/pt-BR/docs/install#debian-stable))
[![create-react-app](https://img.shields.io/badge/create--react--app-3.0.0-orange.svg)]([https://tableless.com.br/criando-sua-aplicacao-react-em-2-minutos/](https://tableless.com.br/criando-sua-aplicacao-react-em-2-minutos/))
[![ws](https://img.shields.io/badge/ws-6.0.0-red.svg)](https://www.npmjs.com/package/ws)

Esse é um código para processar mensagens enviadas ao servidor via WEBSocket.

### Para baixar dependências:
    yarn install

### Para executar:
    yarn start
    
URL: [ws://localhost:3030](ws://localhost:3030)

## Para gerar build:

    yarn build


# Tutorial
### Para exibir horário do servidor:
    /time

### Para calcular uma expressão aritmética simples:
    /calc <arithmetic expression>

### Aluno: Jônathas Assunção Alves - 201613279
